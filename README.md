# Netguru

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Useful information

    * API uses Google oauth so you need to obtain Google credentials from Developer 
    
Routes currently available in the API are:
```
   auth_path  GET     /auth/:provider           NetguruWeb.AuthController :request
   auth_path  GET     /auth/:provider/callback  NetguruWeb.AuthController :callback
   auth_path  GET     /api/authors              NetguruWeb.AuthController :index
   auth_path  GET     /api/authors/:id/edit     NetguruWeb.AuthController :edit
   auth_path  GET     /api/authors/new          NetguruWeb.AuthController :new
   auth_path  GET     /api/authors/:id          NetguruWeb.AuthController :show
   auth_path  POST    /api/authors              NetguruWeb.AuthController :create
   auth_path  PATCH   /api/authors/:id          NetguruWeb.AuthController :update
              PUT     /api/authors/:id          NetguruWeb.AuthController :update
   auth_path  DELETE  /api/authors/:id          NetguruWeb.AuthController :delete
article_path  GET     /api/articles             NetguruWeb.ArticleController :index
article_path  GET     /api/articles/:id/edit    NetguruWeb.ArticleController :edit
article_path  GET     /api/articles/new         NetguruWeb.ArticleController :new
article_path  GET     /api/articles/:id         NetguruWeb.ArticleController :show
article_path  POST    /api/articles             NetguruWeb.ArticleController :create
article_path  PATCH   /api/articles/:id         NetguruWeb.ArticleController :update
              PUT     /api/articles/:id         NetguruWeb.ArticleController :update
article_path  DELETE  /api/articles/:id         NetguruWeb.ArticleController :delete
```