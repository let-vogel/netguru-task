defmodule Netguru.Repo.Migrations.UpdateAuthorArticleRelation do
  use Ecto.Migration

  def change do
    alter table(:articles) do
      add :author_id, references(:authors, on_delete: :delete_all)
    end
  end
end
