defmodule Netguru.Repo.Migrations.AddAuthenticationSystem do
  use Ecto.Migration

  def change do
    alter table(:authors) do
      add :email, :string
      add :passwd_hash, :string
    end
  end
end
