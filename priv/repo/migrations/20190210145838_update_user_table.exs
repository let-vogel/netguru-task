defmodule Netguru.Repo.Migrations.UpdateUserTable do
  use Ecto.Migration

  def change do
    alter table(:authors) do
      add :token, :string
      remove :passwd_hash 
    end
  end
end
