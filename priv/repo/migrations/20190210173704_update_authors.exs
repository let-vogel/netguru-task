defmodule Netguru.Repo.Migrations.UpdateAuthors do
  use Ecto.Migration

  def change do
    alter table(:authors) do
      remove :age
    end
  end
end
