defmodule Netguru.Repo.Migrations.CreateAuthorIndex do
  use Ecto.Migration

  def change do
    create index(:articles, [:author_id])
  end
end
