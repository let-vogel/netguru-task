# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :netguru,
  ecto_repos: [Netguru.Repo]

# Configures the endpoint
config :netguru, NetguruWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ouiW97oSZdzVFGxK/jO+zk05cqP5Xf2m9S/lC5ajQjwzT0y+Q19D4QP2uSX28A9r",
  render_errors: [view: NetguruWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Netguru.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :ueberauth, Ueberauth,
  providers: [
    google: {Ueberauth.Strategy.Google, []}
  ]

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: System.get_env("GOOGLE_CLIENT_ID"),
  client_secret: System.get_env("GOOGLE.CLIENT.SECRET")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
