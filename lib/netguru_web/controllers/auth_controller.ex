defmodule NetguruWeb.AuthController do
    use NetguruWeb, :controller
    plug Ueberauth
    alias Netguru.Publications

    
    def index(conn, _params) do
        authors = Publications.list_authors()
        render conn, "index.json", authors: authors
    end

    def callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
        user_params = %{
          first_name: auth.info.first_name,
          last_name: auth.info.last_name,
          token: auth.credentials.token,
          email: auth.info.email,
        }
        changeset = Publications.Author.changeset(%Publications.Author{}, user_params)
      
        signin(conn, changeset)
    end
      
    def signout(conn, _params) do
        conn
        |> configure_session(drop: true)
        |> redirect(to: "/")
    end

    def delete(conn, %{"id" => id}) do
        case Plug.Conn.get_req_header(conn, "authorization") do
            [auth_header] -> 
                {_status, token} = get_token_from_header(auth_header)
                case Publications.delete_author(id, token) do
                    {:ok, _author} ->
                        render conn, "delete.json"
                    {:error, message} ->
                        render conn, "error.json", message: message
                end
            _ -> {:error, :missing_auth_header}
        end 
    end

    def update(conn, %{"id" => id} = params) do
        case Plug.Conn.get_req_header(conn, "authorization") do
            [auth_header] -> 
                {_status, token} = get_token_from_header(auth_header)
                case Publications.update_author(id, params, token) do
                    {:ok, author} ->
                        render conn, "show.json", author: author
                    {:error, message} ->
                        render(conn, "error.json", message: message) 
                end
            _ -> {:error, :missing_auth_header}
         end
      end

    defp signin(conn, changeset) do
        case insert_or_update_user(changeset) do
          {:ok, author} ->
            conn
            |> put_session(:author_id, author.id)
            |> render("show.json", author: author)
          {:error, reason} ->
            render conn, "error.json", error: reason
        end
    end
      
    defp insert_or_update_user(changeset) do
        case Netguru.Repo.get_by(Publications.Author, email: changeset.changes.email) do
            nil ->
              Netguru.Repo.insert(changeset)
            author ->
              {:ok, author}
          end
    end


    defp get_token_from_header(auth_header) do
        {:ok, reg} = Regex.compile("Bearer\:?\s+(.*)$", "i")
        case Regex.run(reg, auth_header) do
          [_, match] -> {:ok, String.trim(match)}
          _ -> {:error, "token not found"}
        end
      end
end