defmodule NetguruWeb.ArticleController do
    use NetguruWeb, :controller
    alias Netguru.Publications

    def index(conn, _params) do
        articles = Publications.list_articles()
        render conn, "index.json", articles: articles
    end

    def show(conn, %{"id" => id}) do
        render conn, "show.json", article: Publications.get_article!(id)
    end

    def delete(conn, %{"id" => id}) do
        case Plug.Conn.get_req_header(conn, "authorization") do
            [auth_header] -> 
                {_status, token} = get_token_from_header(auth_header)
                case Publications.delete_article(id, token) do
                    {:ok, _article} ->
                        conn
                        |> render("delete.json")
                    {:error, message} ->
                        render(conn, "error.json", message: message) 
                end
            _ -> {:error, :missing_auth_header}
        end
    end

    def create(conn, params) do
        case Plug.Conn.get_req_header(conn, "authorization") do
            [auth_header] -> 
                {_status, token} = get_token_from_header(auth_header)
                with {:ok, %Publications.Article{} = article} <- Publications.create_article(params, token) do
                    render conn, "show.json", article: article
                end
            _ -> {:error, :missing_auth_header}
        end
    end

    def update(conn, %{"id" => id} = params) do
        case Plug.Conn.get_req_header(conn, "authorization") do
            [auth_header] -> 
                {_status, token} = get_token_from_header(auth_header)
                case Publications.update_article(id, params, token) do
                    {:ok, article} ->
                        render conn, "show.json", article: article
                    {:error, message} ->
                        render(conn, "error.json", message: message) 
                end
            _ -> {:error, :missing_auth_header}
        end
    end


    defp get_token_from_header(auth_header) do
        {:ok, reg} = Regex.compile("Bearer\:?\s+(.*)$", "i")
        case Regex.run(reg, auth_header) do
            [_, match] -> {:ok, String.trim(match)}
            _ -> {:error, "token not found"}
        end
    end
end