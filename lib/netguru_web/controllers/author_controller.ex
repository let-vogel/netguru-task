defmodule NetguruWeb.AuthorController do
    use NetguruWeb, :controller
    alias Netguru.Publications

    def index(conn, _params) do
        authors = Publications.list_authors()
        render conn, "index.json", authors: authors
    end

    def create(conn, %{"author" => params}) do
        with {:ok, %Publications.Author{} = author} <- Publications.create_author(params),
             {:ok, token, _claims} <- Netguru.Guardian.encode_and_sign(author) do
               render conn, "jwt.json", jwt: token
        end
    end
end