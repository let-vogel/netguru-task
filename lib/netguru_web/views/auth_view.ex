defmodule NetguruWeb.AuthView do
    use NetguruWeb, :view
    alias NetguruWeb.AuthView

    def render("index.json", %{authors: authors}) do
        %{data: render_many(authors, AuthView, "auth.json", as: :author)}
    end
    
    def render("show.json", %{author: author}) do
        %{data: render_one(author, AuthView, "logged_user.json", as: :author)}
    end
    
    def render("delete.json", _) do
        %{message: "Deleted."}
    end

    def render("error.json", %{message: message}) do
        %{message: message}
    end

    def render("auth.json", %{author: author}) do
        %{id: author.id,
          first_name: author.first_name,
          last_name: author.last_name,
          email: author.email}
    end

    def render("logged_user.json", %{author: author}) do
        %{id: author.id,
        first_name: author.first_name,
        last_name: author.last_name,
        email: author.email,
        token: author.token}
    end
end