defmodule Netguru.Publications do
  @moduledoc """
  The Publications context.
  """

  import Ecto.Query, warn: false
  alias Netguru.Repo

  alias Netguru.Publications.Author

  @doc """
  Returns the list of authors.

  ## Examples

      iex> list_authors()
      [%Author{}, ...]

  """
  def list_authors do
    Repo.all(Author)
  end

  @doc """
  Gets a single author.

  Raises `Ecto.NoResultsError` if the Author does not exist.

  ## Examples

      iex> get_author!(123)
      %Author{}

      iex> get_author!(456)
      ** (Ecto.NoResultsError)

  """
  def get_author!(id), do: Repo.get!(Author, id)

  @doc """
  Creates a author.

  ## Examples

      iex> create_author(%{field: value})
      {:ok, %Author{}}

      iex> create_author(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_author(attrs \\ %{}) do
    %Author{}
    |> Author.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a author.

  ## Examples

      iex> update_author(author, %{field: new_value})
      {:ok, %Author{}}

      iex> update_author(author, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_author(authorID, attrs, token) do
    case Netguru.Publications.is_user?(authorID, token) do
        true ->
        Netguru.Publications.get_author!(authorID)
        |> Author.changeset(attrs)
        |> Repo.update()  
        false ->
            {:error, "Access denied"}
    end
  end

  @doc """
  Deletes a Author.

  ## Examples

      iex> delete_author(author)
      {:ok, %Author{}}

      iex> delete_author(author)
      {:error, %Ecto.Changeset{}}

  """
  def delete_author(authorID, token) do
    case Netguru.Publications.is_user?(authorID, token) do
        true ->
        Netguru.Publications.get_author!(authorID)
        |> Repo.delete()
        false ->
            {:error, "Access denied"}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking author changes.

  ## Examples

      iex> change_author(author)
      %Ecto.Changeset{source: %Author{}}

  """
  def change_author(%Author{} = author) do
    Author.changeset(author, %{})
  end

  alias Netguru.Publications.Article

  @doc """
  Returns the list of articles.

  ## Examples

      iex> list_articles()
      [%Article{}, ...]

  """
  def list_articles do
    Repo.all(Article)
  end

  @doc """
  Gets a single article.

  Raises `Ecto.NoResultsError` if the Article does not exist.

  ## Examples

      iex> get_article!(123)
      %Article{}

      iex> get_article!(456)
      ** (Ecto.NoResultsError)

  """
  def get_article!(id), do: Repo.get!(Article, id)

  @doc """
  Creates a article.

  ## Examples

      iex> create_article(%{field: value})
      {:ok, %Article{}}

      iex> create_article(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_article(attrs \\ %{}, token) do
    author = Repo.get_by(Author, token: token)
    %Article{}
    |> Article.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:author, author)
    |> Repo.insert()
  end

  @doc """
  Updates a article.

  ## Examples

      iex> update_article(articleID, %{field: new_value}, token)
      {:ok, %Article{}}

      iex> update_article(article, %{field: bad_value}, token)
      {:error, %Ecto.Changeset{}}

  """
  def update_article(articleID, attrs, token) do
    case Netguru.Publications.is_owner?(articleID, token) do
        true ->
        Netguru.Publications.get_article!(articleID)
        |> Article.changeset(attrs)
        |> Repo.update()  
        false ->
            {:error, "Access denied"}
    end
    # if author_id == owner do
    #     article
    #     |> Article.changeset(attrs)
    #     |> Repo.update()  
    # end

  end

  @doc """
  Deletes a Article.

  ## Examples

      iex> delete_article(article)
      {:ok, %Article{}}

      iex> delete_article(article)
      {:error, %Ecto.Changeset{}}

  """
  def delete_article(articleID, token) do
    case Netguru.Publications.is_owner?(articleID, token) do
        true ->
            article = Netguru.Publications.get_article!(articleID)
            Repo.delete(article)
        false ->
            {:error, "Access denied"}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking article changes.

  ## Examples

      iex> change_article(article)
      %Ecto.Changeset{source: %Article{}}

  """
  def change_article(%Article{} = article) do
    Article.changeset(article, %{})
  end

  def is_owner?(articleID, token) do
    author = Repo.get_by(Author, token: token)
    article = Netguru.Publications.get_article!(articleID)

    author.id == article.author_id
  end

  def is_user?(authorID, token) do
    author = Repo.get_by(Author, token: token)
    user = Netguru.Publications.get_author!(authorID)

    author.id == user.id
  end
end

