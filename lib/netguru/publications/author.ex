defmodule Netguru.Publications.Author do
  use Ecto.Schema
  import Ecto.Changeset


  schema "authors" do
    # field :age, :integer
    field :first_name, :string
    field :last_name, :string
    field :email, :string
    field :token, :string
    has_many :articles, Netguru.Publications.Article

    # Virtual fields:
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    timestamps()
  end

  @doc false
  def changeset(author, attrs) do
    author
    |> cast(attrs, [:first_name, :last_name, :email, :token])
    |> validate_required([:first_name, :last_name, :email, :token])
    |> validate_format(:email, ~r/@/)
    # |> validate_number(:age, greater_than_or_equal_to: 13)
    |> unique_constraint(:email)
  end

end
