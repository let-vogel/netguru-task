## DISCLAIMER: I'm having a problem with making some of the test green here. Even if the assert *looks* okay (as you can see from IO.inspect), I'm getting an error about
## Ecto being unable to cast a value. I'm not sure why it is like that and I would love to tackle this issue during a pair programming/interview.

defmodule Netguru.PublicationsTest do
  use Netguru.DataCase

  alias Netguru.Publications

  describe "authors" do
    alias Netguru.Publications.Author

    @valid_attrs %{first_name: "some first_name", last_name: "some last_name", email: "test@email.com", token: "aaaa"}
    @update_attrs %{first_name: "some updated first_name", last_name: "some updated last_name", email: "new@email.com", token: "aaaa"}
    @invalid_attrs %{age: nil, first_name: nil, last_name: nil}

    def author_fixture(attrs \\ %{}) do
      {:ok, author} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Publications.create_author()

      author
    end

    test "list_authors/0 returns all authors" do
      author = author_fixture()
      assert Publications.list_authors() == [author]
    end

    test "get_author!/1 returns the author with given id" do
      author = author_fixture()
      assert Publications.get_author!(author.id) == author
    end

    test "create_author/1 with valid data creates a author" do
      assert {:ok, %Author{} = author} = Publications.create_author(@valid_attrs)
      assert author.first_name == "some first_name"
      assert author.last_name == "some last_name"
    end

    test "create_author/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Publications.create_author(@invalid_attrs)
    end

    test "update_author/3 with valid data updates the author" do
      author = author_fixture()
      assert {:ok, author} = Publications.update_author(author.id, @update_attrs, author.token)
      assert %Author{} = author
      assert author.token == "aaaa"
      assert author.first_name == "some updated first_name"
      assert author.last_name == "some updated last_name"
    end

    test "update_author/3 with invalid data returns error changeset" do
      author = author_fixture()
      assert {:error, %Ecto.Changeset{}} = Publications.update_author(author.id, @invalid_attrs, author.token)
      assert author == Publications.get_author!(author.id)
    end

    test "delete_author/2 deletes the author" do
      author = author_fixture()
      assert {:ok, %Author{}} = Publications.delete_author(author.id, author.token)
      assert_raise Ecto.NoResultsError, fn -> Publications.get_author!(author.id) end
    end

  end

  describe "articles" do
    alias Netguru.Publications.Article

    @valid_attrs %{body: "some body", description: "some description", published_date: ~N[2010-04-17 14:00:00.000000], title: "some title"}
    @update_attrs %{body: "some updated body", description: "some updated description", published_date: ~N[2011-05-18 15:01:01.000000], title: "some updated title"}
    @invalid_attrs %{body: nil, description: nil, published_date: nil, title: nil}

    def article_fixture(attrs \\ %{}) do
      {:ok, article} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Publications.create_article()

      article
    end

    test "list_articles/0 returns all articles" do
      article = article_fixture()
      assert Publications.list_articles() == [article]
    end

    test "get_article!/1 returns the article with given id" do
      article = article_fixture()
      assert Publications.get_article!(article.id) == article
    end

    test "create_article/1 with valid data creates a article" do
      assert {:ok, %Article{} = article} = Publications.create_article(@valid_attrs)
      assert article.body == "some body"
      assert article.description == "some description"
      assert article.published_date == ~N[2010-04-17 14:00:00.000000]
      assert article.title == "some title"
    end

    test "create_article/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Publications.create_article(@invalid_attrs)
    end

    test "update_article/3 with valid data updates the article" do
      article = article_fixture()
      author = author_fixture()
      assert {:ok, article} = Publications.update_article(article.id, @update_attrs, author.token)
      assert %Article{} = article
      assert article.body == "some updated body"
      assert article.description == "some updated description"
      assert article.published_date == ~N[2011-05-18 15:01:01.000000]
      assert article.title == "some updated title"
    end

    test "update_article/3 with invalid data returns error changeset" do
      article = article_fixture()
      author = author_fixture()
      assert {:error, %Ecto.Changeset{}} = Publications.update_article(article.id, @invalid_attrs, author.id)
      assert article == Publications.get_article!(article.id)
    end

    test "delete_article/2 deletes the article" do
      article = article_fixture()
      author = author_fixture()
      assert {:ok, %Article{}} = Publications.delete_article(article.id, author.id)
      assert_raise Ecto.NoResultsError, fn -> Publications.get_article!(article.id) end
    end

  end
end
